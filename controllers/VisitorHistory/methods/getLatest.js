/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  limit = require('./../../../config').get('pagination:perPageForLatest'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var VisitorHistory = db.model('VisitorHistory');
var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'VisitorHistory:latest';

module.exports = function (callback) {
  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }


    VisitorHistory.findAll({
        limit: limit,
        order: [['createdAt', 'DESC']]
      })
      .then(function (result) {
        cache.set(cacheKey, result);
        callback(null, result);
      })
      .catch(function (error) {
        console.error(error);
        callback(error);
      });
  });
};