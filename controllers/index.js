/* jshint strict: true */

'use strict';

module.exports.Auth = require('./Auth');
module.exports.Artists = require('./Artists');
module.exports.Tracks = require('./Tracks');
module.exports.VisitorHistory = require('./VisitorHistory');