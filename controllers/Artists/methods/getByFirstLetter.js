/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  md5 = require('md5'),
  config = require('./../../../config'),
  perPage = config.get('pagination:perPage'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var Artist = db.model('Artist');

module.exports = function (letter, page, callback) {
  var cacheKey = 'Artist:byFirstLetter:' + letter+':page:'+page;
  
  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }
    
    if (!_.isEmpty(result)) {
      return callback(null, result);
    }
    
    var query = {};
    if(letter.length == 1) {
      query.name = {
        $like: letter+'%'
      }
    }
    query.active = true;
    query.deleted = false;
  
    var offset = 0;
    page = parseInt(page);
    if(page > 1) {
      offset = perPage * (page-1);
    }
    
    Artist.findAndCountAll({
        where: query,
        order: [
          ['name', 'ASC']
        ],
        offset: offset,
        limit: perPage
      })
      .then(function (result) {
        cache.set(cacheKey, result);
        callback(null, result);
      })
      .catch(function (err) {
        console.error(err);
        callback(err);
      });
  });
};