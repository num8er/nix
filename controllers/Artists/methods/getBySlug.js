/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  md5 = require('md5'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var Artist = db.model('Artist');

module.exports = function (slug, callback) {
  var cacheKey = 'Artist:bySlug:' + slug;
  
  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }
    
    if (!_.isEmpty(result)) {
      return callback(null, result);
    }
    
    var query = {
      slugHash: md5(slug),
      active: true,
      deleted: false
    };
    
    Artist.findOne({
        where: query
      })
      .then(function (result) {
        cache.set(cacheKey, result);
        callback(null, result);
      })
      .catch(function (err) {
        console.error(err);
        callback(err);
      });
  });
};