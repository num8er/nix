/* jshint strict: true */

'use strict';

module.exports.getByFirtsLetter = require('./methods/getByFirstLetter');
module.exports.getBySlug = require('./methods/getBySlug');