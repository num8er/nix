/* jshint strict: true */

'use strict';

module.exports.getAll = require('./methods/getAll');
module.exports.getLatest = require('./methods/getLatest');
module.exports.getRandom = require('./methods/getRandom');
module.exports.getMostPlayed = require('./methods/getMostPlayed');
module.exports.getMostDownloaded = require('./methods/getMostDownloaded');
module.exports.getPopular = require('./methods/getPopular');
module.exports.getBySlug = require('./methods/getBySlug');
module.exports.getArtistTracksByTrackSlug = require('./methods/getArtistTracksByTrackSlug');
module.exports.getArtistTracksByArtistSlug = require('./methods/getArtistTracksByArtistSlug');
module.exports.search = require('./methods/search');
module.exports.incrementById = require('./methods/incrementById');