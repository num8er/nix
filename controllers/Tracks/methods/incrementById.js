/* jshint strict: true */

'use strict';

var
  db = require('./../../../components/database'),
  Track = db.model('Track');

module.exports = function (id, field) {
  Track
    .findById(id)
    .then(function (track) {
      track.increment(field);
    })
    .catch(function (err) {
      console.error(err);
    });
};