/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  md5 = require('md5'),
  limit = require('./../../../config').get('pagination:perPage'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager'),
  getBySlug = require('./getBySlug');

var Track = db.model('Track');
var Artist = db.model('Artist');

module.exports = function (slug, callback) {
  var cacheKey = 'Tracks:artistTracksByTrackSlug:' + slug;
  
  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    getBySlug(slug, function (err, result) {
      if (err) {
        console.error(err);
      }

      if (_.isEmpty(result)) {
        return callback();
      }

      var query = {
        artistId: result.artistId,
        approved: true,
        active: true,
        deleted: false
      };

      Track.findAll({
          include: [Artist],
          where: query,
          limit: limit,
          order: [['createdAt', 'DESC']]
        })
        .then(function (result) {
          cache.set(cacheKey, result);
          callback(null, result);
        })
        .catch(function (err) {
          console.error(err);
          callback(err);
        });
    });
  });
};