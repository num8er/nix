/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'Tracks:all';

module.exports = function (callback) {
  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    var query = {
      approved: true,
      active: true,
      deleted: false
    };

    Track.findAll({
        include: [Artist],
        where: query
      })
      .then(function (result) {
        cache.set(cacheKey, result);
        callback(null, result);
      })
      .catch(function (err) {
        console.error(err);
        callback(err);
      });
  });
};