/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  async = require('async'),
  config = require('./../../../config'),
  perPage = config.get('pagination:perPageForSearch'),
  maxSearchResult = require('./../../../config').get('pagination:maxSearchResult'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager'),
  findTrackIds = require('./../../Index').findTrackIds;

var Track = db.model('Track');
var Artist = db.model('Artist');

module.exports = function (q, page, callback) {
  var words = q.split(' ');
  var cacheKey = 'Tracks:search' + ':q:' + words.join(':') + ':perPage:'+perPage+':page:'+page;

  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    findTrackIds(words, function (err, trackIds) {
      if(err || !trackIds) {
        trackIds = [];
      }
      
      var query = {
        id: {
          $in: trackIds
        },
        approved: true,
        active: true,
        deleted: false
      };
  
      var offset = 0;
      page = parseInt(page);
      if(page > 1) {
        offset = perPage * (page-1);
      }
      
      Track.findAndCountAll({
          include: [Artist],
          where: query,
          offset: offset,
          limit: perPage
        })
        .then(function (result) {
          cache.set(cacheKey, result);
          callback(null, result);
        })
        .catch(function (err) {
          console.error(err);
          callback(err);
        });
    });
  });
};