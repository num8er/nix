/* jshint strict: true */

'use strict';

var
  Sequelize = require('sequelize'),
  cache = require('./../../../components/cache-manager'),
  limit = require('./../../../config').get('pagination:perPageForRandom'),
  db = require('./../../../components/database');

var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'Tracks:random';

module.exports = function (callback) {
  cache.get(cacheKey, function (err, result) {
    if(err) {
      console.error(err);
    }
  
    if(result) {
      return callback(null, result);
    }
    
    var query = {
      approved: true,
      active: true,
      deleted: false
    };
  
    Track.findAll({
           include: [Artist],
           where: query,
           limit: limit,
           order: [Sequelize.fn('RAND')]
         })
         .then(function(result) {
           cache.set(cacheKey, result);
           callback(null, result);
         })
         .catch(function(err) {
           console.error(err);
           callback(err);
         });
  });
};