/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  md5 = require('md5'),
  config = require('./../../../config'),
  perPage = config.get('pagination:perPage'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager'),
  getArtistBySlug = require('./../../Artists').getBySlug;

var Track = db.model('Track');

module.exports = function (slug, page, callback) {
  var cacheKey = 'Tracks:artistTracksByArtistSlug:' + slug+':page:'+page;
  
  cache.get(cacheKey, function (err, result) {
    if (err) {
      console.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }
  
    getArtistBySlug(slug, function (err, result) {
      if (err) {
        console.error(err);
      }

      if (_.isEmpty(result)) {
        return callback();
      }
      
      var query = {
        artistId: result.id,
        approved: true,
        active: true,
        deleted: false
      };
  
      var offset = 0;
      page = parseInt(page);
      if(page > 1) {
        offset = perPage * (page-1);
      }
      
      Track.findAndCountAll({
          where: query,
          offset: offset,
          limit: perPage,
          order: [['name', 'ASC']]
        })
        .then(function (result) {
          cache.set(cacheKey, result);
          callback(null, result);
        })
        .catch(function (err) {
          console.error(err);
          callback(err);
        });
    });
  });
};