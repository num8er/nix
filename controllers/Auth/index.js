/* jshint strict: true */

'use strict';

module.exports.attempt = require('./methods/attempt');
module.exports.destroy = require('./methods/destroy');