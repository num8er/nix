/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  db = require('./../../../components/database');

module.exports = function (query, callback) {
  var User = db.model('User');

  query = _.pick(query, ['username', 'password']);
  User.findOne({where: query, limit: 1})
    .then(function (result) {
      if (_.isEmpty(result)) {
        return callback({
          statusCode: 403,
          success: false,
          error: 'Forbidden'
        });
      }

      callback(null, {
        statusCode: 200,
        success: true
      });
    })
    .catch(function (error) {
      callback({
        statusCode: 500,
        success: false,
        error: error
      });
    });
};