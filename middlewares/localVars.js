/* jshint strict: true */

'use strict';

module.exports = function (req, res, next) {
  for (var key in req.params) res.locals[key] = req.params[key];
  for (var key in req.query)  res.locals[key] = req.query[key];
  next();
};