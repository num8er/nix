/* jshint strict: true */

'use strict';

var _ = require('lodash');

module.exports = function (req, res, next) {
  res.locals['q'] = res.locals['q'] || '';
  res.locals['q'] = _.trim(res.locals['q']);
  req.query['q'] = res.locals['q'];

  if(parseInt(res.locals['page']) < 1) {res.locals['page'] = 1;}
  req.query['page'] = res.locals['page'];

  next();
};