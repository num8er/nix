/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  Sequelize = require('sequelize'),
  db = require('./../components/database');

var Keyword = db.model('Keyword');

module.exports = function (req, res, next) {
  if(
    req.headers['user-agent'] && (
      req.headers['user-agent'].toLowerCase().indexOf('bot') > -1 ||
      req.headers['user-agent'].toLowerCase().indexOf('spider') > -1 ||
      req.headers['user-agent'].toLowerCase().indexOf('crawl') > -1
    )
  ) {
    Keyword
      .findAll({
        limit: 20,
        order: [Sequelize.fn('RAND')]
      })
      .then(function (result) {
        res.locals.keywords = _.map(result, function(record) {
          return {
            title: record.keyword,
            slug: _.kebabCase(record.keyword)
          };
        });
      })
      .catch(function (err) {
        console.log(err);
      })
      .finally(function() {
        next();
      });
    return;
  }
  
  next();
};