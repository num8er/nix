/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  async = require('async'),
  express = require('express'),
  router = express.Router(),
  db = require('./../../components/database');

var Keyword = db.model('Keyword');

router.get('/', function (req, res) {
  res.render('backend/keywords');
});

router.post('/', function (req, res) {
  async.each(req.body.keywords.split("\n"), function(keyword, next) {
    keyword = _.trim(keyword);
    Keyword
      .upsert({keyword})
      .then(function () {
        next();
      });
  });
  
  res.redirect('back');
});

module.exports = router;