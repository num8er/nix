/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks;

router.get('/', function (req, res) {
  var q = _.trim(req.query.q);
  var page = req.query.page;
  
  if(q.length < 2) {
    return res.redirect('back');
  }
  
  async.parallel([
      function (next) {
        TracksController.search(q, page, next);
      },
      TracksController.getLatest
    ],
    function (err, results) {
      var data = {
        count: (!_.isEmpty(results[0])? results[0].count : 0),
        tracks: (!_.isEmpty(results[0])? results[0].rows : []),
        latestTracks: results[1]
      };

      res.render('website/search', data);
    });
});

module.exports = router;