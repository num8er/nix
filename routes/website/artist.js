/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks,
  ArtistController = controllers.Artists;

router.get('/:slug', function (req, res) {
  var slug = req.params.slug;
  var page = req.query.page;
  
  async.parallel([
      function (next) {
        ArtistController.getBySlug(slug, next);
      },
      function (next) {
        TracksController.getArtistTracksByArtistSlug(slug, page, next);
      }
    ],
    function (err, results) {
      
      var data = {
        artist: results[0],
        count: (!_.isEmpty(results[1])? results[1].count : 0),
        tracks: (!_.isEmpty(results[1])? results[1].rows : [])
      };
      
      if(data.count < 1) {
        return res.redirect('back');
      }
      
      res.render('website/artist', data);
    });
});

module.exports = router;