/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  util = require('util'),
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  cache = require('./../../components/cache-manager'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks;

function played(track, req) {
  let cacheKey = ['Track', 'played', req.session.id, track.id].join(':');
  cache.get(cacheKey, function(err, result) {
    if(result) {
      return;
    }
    
    cache.set(cacheKey, 1);
    TracksController.incrementById(track.id, 'played');
  });
}

function downloaded(track, req) {
  let cacheKey = ['Track', 'downloaded', req.session.id, track.id].join(':');
  cache.get(cacheKey, function(err, result) {
    if(result) {
      return;
    }
    
    cache.set(cacheKey, 1);
    TracksController.incrementById(track.id, 'downloaded');
  });
}

router.get('/:slug', function (req, res) {
  let slug = req.params.slug;
  
  async.parallel([
      function (next) {
        TracksController.getBySlug(slug, next);
      },
      function (next) {
        TracksController.getArtistTracksByTrackSlug(slug, next);
      },
      TracksController.getRandom
    ],
    function (err, results) {
      let data = {
        track: results[0],
        artistTracks: results[1],
        randomTracks: results[2]
      };
      
      if(_.isEmpty(data.track)) {
        return res.redirect('back');
      }
      
      res.render('website/track', data);
    });
});

router.get('/:slug/download', function (req, res) {
  let slug = req.params.slug;
  TracksController.getBySlug(slug, function(err, track) {
    downloaded(track, req);
    
    let file = path.join(
      __dirname,
      '/../../storage/tracks/',
      track.folder, track.uuid, 'original.mp3'
    );
    
    res.download(file, track.Artist.name + ' - ' + track.name + '.mp3');
  });
});

function getFileStat(file, callback) {
  let cacheKey = ['File', 'stat', file].join(':');
  
  cache.get(cacheKey, function(err, stat) {
    if(stat) {
      return callback(null, stat);
    }
    
    fs.stat(file, function(err, stat) {
      if(err) {
        return callback(err);
      }
      
      cache.set(cacheKey, stat);
      callback(null, stat);
    });
  });
}

class FileReadStreams {
  constructor() {
    this._streams = {};
  }
  
  make(file, options = null) {
    return options ?
      fs.createReadStream(file, options)
      : fs.createReadStream(file);
  }
  
  get(file) {
    return this._streams[file] || this.set(file);
  }
  
  set(file) {
    return this._streams[file] = this.make(file);
  }
}
const readStreams = new FileReadStreams();

function streamFile(file, req, res) {
  getFileStat(file, function(err, stat) {
    if(err) {
      console.error(err);
      return res.status(404);
    }
    
    let bufferSize = 1024 * 1024;
    res.writeHead(200, {
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      'Pragma': 'no-cache',
      'Expires': 0,
      'Content-Type': 'audio/mpeg',
      'Content-Length': stat.size
    });
    readStreams.make(file, {bufferSize}).pipe(res);
  });
}

function streamFileChunked(file, req, res) {
  getFileStat(file, function(err, stat) {
    if(err) {
      console.error(err);
      return res.status(404);
    }
    
    let chunkSize = 1024 * 1024;
    if(stat.size > chunkSize * 2) {
      chunkSize = Math.ceil(stat.size * 0.25);
    }
    let range = (req.headers.range) ? req.headers.range.replace(/bytes=/, "").split("-") : [];
    
    range[0] = range[0] ? parseInt(range[0], 10) : 0;
    range[1] = range[1] ? parseInt(range[1], 10) : range[0] + chunkSize;
    if(range[1] > stat.size - 1) {
      range[1] = stat.size - 1;
    }
    range = {start: range[0], end: range[1]};
    
    let stream = readStreams.make(file, range);
    res.writeHead(206, {
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      'Pragma': 'no-cache',
      'Expires': 0,
      'Content-Type': 'audio/mpeg',
      'Accept-Ranges': 'bytes',
      'Content-Range': 'bytes ' + range.start + '-' + range.end + '/' + stat.size,
      'Content-Length': range.end - range.start + 1,
    });
    stream.pipe(res);
  });
}

router.get('/:slug/stream', function (req, res) {
  let slug = req.params.slug;
  TracksController.getBySlug(slug, function(err, track) {
    played(track, req);
    
    let file = path.join(
      __dirname,
      '/../../storage/tracks/',
      track.folder, track.uuid, 'original.mp3'
    );
    
    if(/firefox/i.test(req.headers['user-agent'])) {
      return streamFile(file, req, res);
    }
    streamFileChunked(file, req, res);
  });
});

module.exports = router;