/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  controllers = require('./../../controllers'),
  ArtistsController = controllers.Artists;

router.get('/:letter?', function (req, res) {
  var letter = _.trim(req.params.letter);
  var page = req.query.page;
  
  ArtistsController.getByFirtsLetter(letter, page, function (err, result) {
    if(result.count < 1) {
      return res.redirect('back');
    }
    
    res.render('website/artists', {
      count: result.count,
      artists: result.rows
    });
  });
});

module.exports = router;