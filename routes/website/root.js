/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  sitemap = require('sitemap'),
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks;

router.get('/', function (req, res) {
  async.parallel([
      TracksController.getRandom,
      TracksController.getMostPlayed
    ],
    function (err, results) {
      var data = {
        randomTracks: results[0],
        mostPlayedTracks: results[1]
      };
      res.render('website/index', data);
    });
});

router.get('/:keyword\.html', function(req, res) {
  TracksController.getRandom(function(err, tracks) {
    res.render('website/keyword', {
      keyword: req.params.keyword.split('-').join(' '),
      tracks
    });
  });
});

function searchHandler(req, res) {
  var
    q = _.trim(req.params.q.split('-').join(' ')),
    page = req.query.page;
  
  if(q.length < 2) {
    return res.redirect('back');
  }
  
  async.parallel([
      function (next) {
        TracksController.search(q, page, next);
      },
      TracksController.getLatest
    ],
    function (err, results) {
      var data = {
        count: results[0].count,
        tracks: results[0].rows,
        latestTracks: results[1]
      };
      
      res.render('website/search', data);
    });
}
router.get('/:q', searchHandler);
router.get('/:q/ts*', searchHandler);

module.exports = router;