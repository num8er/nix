/* jshint strict: true */

'use strict';

var
  express = require('express'),
  router = express.Router(),
  requestHelper = require('./../../components/request-helper'),
  controllers = require('./../../controllers'),
  AuthController = controllers.Auth;

router.post('/',
  requestHelper.validation({
    body: {
      username: "string|required",
      password: "string|required"
    }
  }),
  function (req, res) {
    AuthController.attempt(req.body, res.defaultResponse);
  });

router.get('/destroy', function (req, res) {
  AuthController.destroy(res.defaultResponse);
});

module.exports = router;