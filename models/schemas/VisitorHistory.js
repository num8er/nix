/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'visitor_history',
    timestamps: false
  },
  fields: {
    type: {
      type: Sequelize.STRING(12),
      allowNull: false
    },
    trackId: {
      type: Sequelize.INTEGER(10),
      field: 'track_id',
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    }
  }
};