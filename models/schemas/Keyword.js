/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'keywords',
    timestamps: false
  },
  fields: {
    keyword: {
      type: Sequelize.STRING(200),
      primaryKey: true,
      allowNull: false
    }
  }
};