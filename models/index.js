/* jshint strict: true */

'use strict';

module.exports.Artist = require('./schemas/Artist');
module.exports.Category = require('./schemas/Category');
module.exports.Index = require('./schemas/Index');
module.exports.Track = require('./schemas/Track');
module.exports.User = require('./schemas/User');
module.exports.VisitorHistory = require('./schemas/VisitorHistory');
module.exports.Keyword = require('./schemas/Keyword');