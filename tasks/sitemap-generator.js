var
  _ = require('lodash'),
  fs = require('fs'),
  sitemap = require('sitemap'),
  async = require('async'),
  controllers = require('./../controllers'),
  TracksController = controllers.Tracks;


async.forever(
  function(next) {
    TracksController.getAll(function(err, tracks) {
      if(err) {
        setTimeout(next, 60*60*1000);
        return;
      }
    
      tracks = _.map(tracks, function(track) {
        return {
          url: '/track/'+track.slug,
          changefreq: 'daily',
          priority: 0.9,
          img: '/data/artists/'+track.Artist.uuid+'/256x256.jpg'
        }
      });
    
      sitemap
        .createSitemap ({
          hostname: 'http://nix.fm',
          cacheTime: 0,
          urls: tracks
        })
        .toXML(function(err, xml) {
          if(err) {
            setTimeout(next, 60*60*1000);
            return;
          }
          
          fs.writeFile(
            __dirname+'/../public/sitemap.xml',
            xml,
            function() {
              setTimeout(next, 60*60*1000);
            });
        });
    });
  });
