/* jshint strict: true */

'use strict';

var
  Memcached = require('memcached'),
  config = require('../config/');

var client = new Memcached(
  config.get('memcache:host')+':'+config.get('memcache:port'),
  {poolSize: 1024, remove: true});

module.exports = client;