/* jshint strict: true */

'use strict';

var
  config = require('../config'),
  cluster = require('cluster'),
  numCPUs = require('os').cpus().length;


module.exports.start = function (callable) {
  var instances = config.get('app:useCluster') ? numCPUs : 1;

  if (instances == 1) {
    return callable();
  }

  if (cluster.isMaster) {
    console.log('Starting', instances, 'instances');
    for (var i = 0; i < instances; i++) {
      cluster.fork();
    }

    cluster.on('exit', function (worker, code, signal) {
      console.log('CLUSTER EXIT', code, signal);
    });
  } else {
    callable();
  }
};