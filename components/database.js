/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  config = require('./../config'),
  winston = require('winston'),
  Sequelize = require('sequelize'),
  models = require('./../models');

var Database = function () {
};

Database.prototype.connection = null;

Database.prototype.getConnectionString = function () {
  return config.get('db:driver') +
    '://' + config.get('db:user') + ':' + config.get('db:pass') +
    '@' + config.get('db:host') + ':' + config.get('db:port')
    + '/' + config.get('db:name');
};

Database.prototype.getConnectionOptions = function () {
  var options = {
    logging: false,
    dialectOptions: {}
  };
  return options;
};

Database.prototype.createConnection = function () {
  this.connection =
    new Sequelize(
      this.getConnectionString(),
      this.getConnectionOptions());
  if (this.connection) {
    winston.info('Database connected');
  }
  return this.connection;
};

Database.prototype.getDefaultSchemaParams = function () {
  return {
    timestamps: true,
    underscored: true,
    freezeTableName: true
  };
};

var ModelInstances = {
  instances: [],
  keep: function (name, instance) {
    this.instances[name] = instance;
  },
  get: function (name) {
    return this.instances[name] || null;
  }
};

Database.prototype.init = function () {
  if (_.isEmpty(this.connection)) {
    this.createConnection();
  }

  return this;
};

Database.prototype.initModels = function () {
  var self = this;

  _.each(models, function (model, name) {
    var params = _.extend(self.getDefaultSchemaParams(), model.params);
    var fields = model.fields;

    var modelInstance = self.connection.define(name, fields, params);
    ModelInstances.keep(name, modelInstance);
  });

  self.initModelRelations();
  return self;
};

Database.prototype.initModelRelations = function () {
  var self = this;

  _.each(models, function (model, name) {
    var associations = model.associations || [];
    if (_.isEmpty(associations)) {
      return;
    }

    var modelInstance = ModelInstances.get(name);
    associations.forEach(function (association) {
      switch (association.type) {
        case 'hasOne' :
          modelInstance.hasOne(self.model(association.model),
            {as: association.as, foreignKey: association.foreignKey});
          break;

        case 'hasMany' :
          modelInstance.hasMany(self.model(association.model),
            {as: association.as, foreignKey: association.foreignKey});
          break;

        case 'belongsTo' :
          modelInstance.belongsTo(self.model(association.model));
          break;
      }
    });

    ModelInstances.keep(name, modelInstance);
  });
  return this;
};

Database.prototype.model = function (name) {
  return ModelInstances.get(_.capitalize(name));
};

Database.prototype.createModel = function (name) {
  return this.model(name);
};

module.exports = (new Database()).init().initModels();