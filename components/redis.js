/* jshint strict: true */

'use strict';

var
  redis = require('redis'),
  winston = require('winston'),
  config = require('../config/');

var client = redis.createClient(config.get('redis:port'), config.get('redis:host'));

client.on('connect', function () {
  winston.info('Redis connected');
});
client.on('error', function (error) {
  winston.error(error);
});

module.exports = client;